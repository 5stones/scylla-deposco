<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/5stones/scylla-deposco/compare/v1.0.0...v1.0.1) (2021-01-29)


### Bug Fixes

* **task.py:** use try/except to catch keyerror ([73fe6b8](https://gitlab.com/5stones/scylla-deposco/commit/73fe6b8))


### Features

* **tasks.py:** link BCOrder to DPSalesorder ([6fca9e3](https://gitlab.com/5stones/scylla-deposco/commit/6fca9e3))



