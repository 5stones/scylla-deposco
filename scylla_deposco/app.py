from scylla import app
from scylla import configuration
from .client import DeposcoRestClient
from . import tasks


configuration.setDefaults('Deposco', {
    'debug': False,
    'timezone': 'US/Central',
    'log_level': '20' #info. 10 for debug
})


class App(app.App):

    def _prepare(self):
        config = configuration.getSection('Deposco')
        client = DeposcoRestClient(
            config['name'],
            config['url'],
            config['username'],
            config['password'],
            config['tenant_code'],
        )
        self.tasks['Salesorder'] = tasks.DeposcoSalesorderTask(
            client,
            'Salesorder',
            record_prefix=config.get('name', None),
        )

        self.tasks['Item'] = tasks.DeposcoItemTask(
            client,
            'Item',
            record_prefix=config.get('name', None),
            deposco_timezone=config.get('timezone'),
        )

        self.tasks['Shipment'] = tasks.DeposcoShipmentTask(
            client,
            'Shipment',
            record_prefix=config.get('name', None),
            deposco_timezone=config.get('timezone'),
        )
