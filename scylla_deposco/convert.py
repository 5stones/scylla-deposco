from datetime import datetime, timedelta
import pytz
from dateutil import parser

def to_timezone(value, timezone='US/Central', datetime_format='%Y-%m-%dT%H:%M:%S%z'):
    """Convert a timestamp to a timestamp in another timezone"""
    if value is None:
        return None

    if not isinstance(value, datetime):
        value = parser.parse(value)

    # if the datetime is not timezone aware, assume utc
    if not value.tzinfo:
        value = pytz.utc.localize(value)

    # convert the datetime to another
    tz = pytz.timezone(timezone)
    value = value.astimezone(tz)
    return value.strftime(datetime_format)


def parse_tz_unaware(value, timezone='US/Central', datetime_format='%Y-%m-%dT%H:%M:%S%z'):
    """Convert an timezone unaware timestamp to a timestamp in the default timezone"""
    if value is None:
        return None

    tz = pytz.timezone(timezone)
    if not isinstance(value, datetime):
        value = parser.parse(value)

    # if the datetime is not timezone aware, assume utc
    if not value.tzinfo:
        value = tz.localize(value)

    return value
