import json
import logging
import requests
from scylla import configuration
from scylla import RestClient
from requests.auth import HTTPBasicAuth

class DeposcoRestError(RestClient.RestError):
    pass


class DeposcoAuthenticationError(DeposcoRestError):
    pass


class DeposcoRestClient(RestClient):
    _logger = logging.getLogger('scylla-deposco')
    _successful_codes = [200, 201, 204]

    RestError = DeposcoRestError

    def __init__(self, name, base_url, username, password, tenant_code):
        self.name = name
        self.base_url = base_url
        self.username = username
        self.password = password
        self.tenant_code = tenant_code
        self.auth = HTTPBasicAuth(self.username, self.password)

        super(DeposcoRestClient, self).__init__(
            base_url
        )

        self._set_header('Content-Type', 'application/json')
        self._set_header('Accept', 'application/json')

    def post(self, path, params=None, data=None):
        endpoint = u'{}/{}/{}'.format(self.base_url, self.tenant_code, path)

        self._logger.debug('POST %s %s', endpoint, (params, data))

        response = requests.post(
            endpoint,
            auth=self.auth,
            params=params,
            json=data,
            **self._base_request
        )

        return self._process_response(response)

    def put(self, path, params=None, data=None):
        endpoint = u'{}/{}/{}'.format(self.base_url, self.tenant_code, path)

        self._logger.debug('PUT %s %s', endpoint, (params, data))

        response = requests.put(
            endpoint,
            auth=self.auth,
            params=params,
            json=data,
            **self._base_request
        )

        return self._process_response(response)

    def get(self, path, params=None, data=None):
        endpoint = u'{}/{}/{}'.format(self.base_url, self.tenant_code, path)

        self._logger.debug('GET %s %s', endpoint, (params, data))

        response = requests.get(
            endpoint,
            auth=self.auth,
            params=params,
            json=data,
            **self._base_request
        )

        return self._process_response(response)

    def _process_response(self, response):
        if response.status_code not in self._successful_codes:
            raise self.RestError(response.text)

        try:
            return response.json()
        except:
            return response
