"""Core connection to Deposco and an App to download updates.

configuration required:
    'Deposco': {
        'name': ...,
        'url': ...,
        'username': ...,
        'password': ...,
        'tenant_code': ...,
        'client': ...,
        'log_level': ...,
    },
"""

from .client import DeposcoRestClient

from .app import App

from .tasks import DeposcoTask, DeposcoSalesorderTask, ToDeposcoTask, ToDeposcoSalesorderTask

from . import records
