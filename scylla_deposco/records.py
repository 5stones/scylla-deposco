from datetime import timedelta
from decimal import Decimal
from scylla import records
from scylla import configuration
from scylla import orientdb

# recursion depth when initializing records
_process_depth = 0

# cache of records cleared after processing a response
_record_cache = {}


class DpRecord(records.ParsedRecord):
    factory_base_classname = 'Dp'
    classname = 'DpRecord'

    _sub_records = {}
    _linked_records = {}
    _readonly_datetime_fields = []
    _datetime_fields = [
        'createdDateTime', 'updatedDateTime',
    ]

    def __init__(self, client, classname, obj, readonly_link=False):
        super(DpRecord, self).__init__(client, classname, obj, readonly_link)
        self.client = client
        self._readonly_link = readonly_link
        self.deposco_module = client.name
        self._default_timezone = configuration.getSection('Deposco').get('timezone')
        if classname:
            self.classname = self.deposco_module + classname

class DpShipment(DpRecord):
    key_field = 'shipmentNumber'

class DpSalesorder(DpRecord):
    key_field = 'number'

    _readonly_datetime_fields = [
        'createdDateTime', 'updatedDateTime',
    ]


class DpItem(DpRecord):
    key_field = 'number'
