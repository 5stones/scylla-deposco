from datetime import datetime
from datetime import timedelta
from scylla import configuration
import math
import logging
import json
import re
import scylla
from scylla import orientdb
from . import records
from .convert import to_timezone

class DeposcoTask(scylla.Task):
    """Downloads records from Deposco."""
    # adjust time buffer for potential discrepencies in Deposco vs our servers
    timing_buffer = 60 * 2

    api_types = {
        'Salesorder': 'search/Order',
        'Item': 'search/item',
        'Shipment': 'search/shipment',
    }

    single_api_types = {
        'Salesorder': 'orders/Sales Order',
        'Item': 'items',
        'Shipment': 'shipments/{}',
    }

    json_field_map = {
        'Salesorder': 'order',
        'Item': 'item',
        'Shipment': 'shipment',
    }

    _logger = logging.getLogger('scylla-deposco')

    def __init__(self, client, record_type, record_prefix='Dp', deposco_timezone='US/Central'):
        self.record_type = record_type.title()
        self.client = client
        self.classname = '{0}{1}'.format(record_prefix, self.record_type)
        self.api_type = self.api_types[self.record_type]
        self.deposco_timezone = deposco_timezone
        self._logger.setLevel(configuration.get('Deposco', 'log_level'))
        self.company = configuration.get('Deposco', 'client')
        self.single_api_types['Shipment'] = self.single_api_types['Shipment'].format(self.company)
        self.single_api_type = self.single_api_types[self.record_type]

        task_id = '{0}-download'.format(self.classname)
        super(DeposcoTask, self).__init__(task_id)

    def _get_show_query_params(self):
        return {}

    def _get_index_query_params(self, after):
        args = {
            'pageSize': 100
        }

        if after:
            formatted_after = to_timezone(after, timezone=self.deposco_timezone)
            args['updatedDate'] = formatted_after[:-5]
            args['updatedDateOps'] = '>'

        return args

    def _step(self, after, options):
        args = self._get_index_query_params(after)

        self._logger.info(u'Getting %s updated after %s', self.api_type, after)
        self.get_all(args)

    def get_all(self, args={}):
        firstAttempt = True
        response = None
        if 'pageNo' not in args or not args['pageNo']:
            args['pageNo'] = 1

        while firstAttempt or (response is not None and len(response[self.json_field_map[self.record_type]]) / float(args['pageSize']) == 1.0):
            if firstAttempt:
                firstAttempt = False
            response = self.client.get('{0}'.format(self.api_type), args)

            if response is not None:
                data = response[self.json_field_map[self.record_type]]
                self._process_search_response(data, isinstance(data, list))
            else:
                self._logger.warning(u"No updated records found.")

            args['pageNo'] += 1

    def process_one(self, rec_id):
        response = self.client.get('{}/{}'.format(self.single_api_type, rec_id), self._get_show_query_params())
        data = response[self.json_field_map[self.record_type]]
        self._process_search_response(data, isinstance(data, list))

    def _process_search_response(self, data, is_collection=True):
        if is_collection == True:
            for obj in data:
                self._process_response_record(obj)
        else:
            self._process_response_record(data)

    def _process_response_record(self, obj):
        rec = records.DpRecord.factory(self.client, self.record_type, obj)
        with scylla.ErrorLogging(self.task_id, rec):
            rec.throw_at_orient()
            self._link_children(rec)
            self._logger.info(
                u'Saved %s %s as %s %s',
                self.record_type,
                obj.get(rec.key_field),
                rec.classname,
                rec.rid
            )

    def _link_children(self, rec):
        """Once the record and subrecords are in orientdb, add _parent to children
            and children of children recursively, depth first.
        """
        # pull rids of children
        rids = []
        for subtype in rec._sub_records:
            value = rec.get(subtype)
            if isinstance(value, records.DpRecord):
                # single record
                rids.append(value.rid)
                # recursively link children of child
                self._link_children(value)
            elif value:
                # list of records
                for subrecord in value:
                    rids.append(subrecord.rid)
                    # recursively link children of child
                    self._link_children(subrecord)
        # run the update in orientdb
        q = "UPDATE [{}] SET _parent = {}".format(','.join(rids), rec.rid)
        orientdb.execute(q)


class DeposcoSalesorderTask(DeposcoTask):

    def _get_show_query_params(self):
        return {
            'type': 'Sales Order',
            'company': self.company,
        }

    def _get_index_query_params(self, after):
        args = {
            'pageSize': 100,
            'type': 'Sales Order',
            'company': self.company,
        }

        if after:
            formatted_after = to_timezone(after, timezone=self.deposco_timezone)
            args['updatedDate'] = formatted_after[:-5]
            args['updatedDateOps'] = '>'

        return args

    def _link_children(self, rec):
        """Draw a `Created` edge from an original BCOrder to a DPSalesorder."""
        super(DeposcoSalesorderTask, self)._link_children(rec)

        if isinstance(rec, records.DpSalesorder):
            try:
                if rec['orderSource'] == 'Bigcommerce':

                    query = (
                        "SELECT @rid AS rid "
                        "FROM BCOrder "
                        "WHERE id = {number} "
                        "AND {rid} NOT IN out('Created') "
                    ).format(
                        number= rec['number'],
                        rid=rec.rid)

                    try:
                        result = orientdb.execute(query)

                        orientdb.execute("CREATE EDGE Created FROM {} TO {}".format(
                            result[0]['rid'], rec.rid
                        ))
                    except IndexError:
                        return None
            except KeyError:
                self._logger.warning(u'Order %s has no source', rec['id'])
                return

class DeposcoItemTask(DeposcoTask):

    def _get_show_query_params(self):
        return {
            'company': self.company,
        }

    def _get_index_query_params(self, after):
        args = {
            'pageSize': 100,
            'company': self.company,
        }

        if after:
            formatted_after = to_timezone(after, timezone=self.deposco_timezone)
            args['updatedDate'] = formatted_after[:-5]
            args['updatedDateOps'] = '>'

        return args

class DeposcoShipmentTask(DeposcoTask):

    def _get_show_query_params(self):
        return {
            'company': self.company,
        }

    def _get_index_query_params(self, after):
        args = {
            'pageSize': 100,
            'company': self.company,
        }

        if after:
            formatted_after = to_timezone(after, timezone=self.deposco_timezone)
            args['actualShipDate'] = formatted_after[:-5]
            args['actualShipDateOps'] = '>'

        return args

    def _link_children(self, rec):
        """Draw a `Shipment` edge from an original Order to a shipment."""
        super(DeposcoShipmentTask, self)._link_children(rec)

        if isinstance(rec, records.DpShipment):
            if rec['orders'] is None or 'order' not in rec['orders']:
                self._logger.warning(u'Shipment %s has no orders', rec['shipmentNumber'])
                return

            orderNumbers = []

            if isinstance(rec['orders']['order'], list):
                for order in rec['orders']['order']:
                    orderNumbers.append("'{}'".format(order['orderNumber']))
            else:
                orderNumbers.append("'{}'".format(rec['orders']['order']['orderNumber']))

            query = (
                "SELECT @rid AS rid "
                "FROM DpSalesorder "
                "WHERE number IN [{order_numbers}] "
                " AND {rid} NOT IN out('Shipment') "
            ).format(
                order_numbers=','.join(orderNumbers),
                rid=rec.rid)

            for row in orientdb.execute(query):
                orientdb.execute("CREATE EDGE Shipment FROM {} TO {}".format(
                    row['rid'], rec.rid
                ))


class ToDeposcoTask(scylla.ReflectTask):
    """Checks for updates to records an pushes them into Deposco.
    """
    link_field = 'number'
    api_types = {
        'Salesorder': 'orders/Sales Order',
        'Item': 'items',
        'Shipment': 'shipments/{}',
    }

    json_field_map = {
        'Salesorder': 'order',
        'Item': 'item',
        'Shipment': 'shipment',
    }

    _logger = logging.getLogger('scylla-deposco')

    def __init__(self,
            from_class,
            conversion,
            client, to_type,
            where=None,
            with_reflection=None):

        super(ToDeposcoTask, self).__init__(
            from_class,
            (client.name, to_type),
            where=where,
            with_reflection=with_reflection)

        self.to_type = to_type
        self.company = configuration.get('Deposco', 'client')
        self.api_types['Shipment'] = self.api_types['Shipment'].format(self.company)
        self.api_type = self.api_types[to_type]
        self.client = client
        self.conversion = conversion

    def _process_response_record(self, obj):
        """Uploads the record to Deposco.
        """

        # create the record in deposco

        data = self.conversion(obj)
        is_update = False
        deposco_result = None
        path = self.api_type + '/' + str(data['number'])

        # do not send orders with no items
        if data['orderLines'] is None or 'orderLine' not in data['orderLines'] or len(data['orderLines']['orderLine']) == 0:
            logging.info('Skipping order ' + str(data['number']) + ' - No Deposco Items')
            return

        data = self._format_data(data)

        self.client.put(path, data=data)
        deposco_result = self.client.get(path)

        # save the deposco response to orient and link it
        deposco_rec = records.DpRecord.factory(self.client, self.to_type, deposco_result[self.json_field_map[self.to_type]][0])
        self._save_response(obj, deposco_rec, is_update, request=data)

    def _format_data(self, data):
        data_obj = {}
        data_obj[self.json_field_map[self.to_type]] = data

        return data_obj


class ToDeposcoSalesorderTask(ToDeposcoTask):
    link_field = 'number'
