from distutils.core import setup
import json

with open('package.json', 'r') as f:
    package_json = json.load(f)

version = package_json['version']

setup(
    name = 'scylla-deposco',
    packages = ['scylla_deposco'],
    version = version,

    description = 'Scylla-Deposco provides the basic utilities to integrate with the standard Deposco API',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-deposco',
    download_url = 'https://gitlab.com/5stones/scylla-deposco/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration scylla deposco',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'requests',
        'requests_oauthlib==1.0.0',
        'oauthlib',
        'pytz',
        'python-dateutil',
        'scylla',
    ],
    dependency_links=[
        'git+https://gitlab.com/5stones/scylla.git',
        'oauthlib',
    ],
)
